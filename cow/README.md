# Cow

## Variant 1

![cow1](./img/cow1.svg)

[Source](https://openclipart.org/detail/158389/cow) 

### Dimension
Wide: ~130mm
Height: ~80mm
Depth: 18mm

### Tools used
- Snapmaker 2.0  
- 1.55mm flat end mill for the pockets job
- 3mm flat end mill with at least 19mm work length for the contour job

### Backside pockets
For milling the backside pockets, you need to turn the part after front pocket and contour job. Then you need to set the origin to the tip of the right horn.

![cow1_backside_origin](./img/cow1_backside_origin.svg)
